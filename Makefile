main: main.zig libparser.a
	zig build-exe -lc -Inimcache -I. -I/home/user/.choosenim/toolchains/nim-#devel/lib/ main.zig libparser.a

libparser.a: parser.nim
	nim c --noMain:on -d:release --nimcache:nimcache --header=parser.h --app:staticLib parser.nim
