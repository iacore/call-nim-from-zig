const std = @import("std");
const c = @cImport({
    @cInclude("parser.h");
});

fn NimStringData(s: c.NimStringV2) []const u8 {
    return @intToPtr([*]u8, @ptrToInt(s.p.*.data()))[0..@intCast(usize, s.len)];
}

fn print_parsed(parsed: []const c.PasswdLine) !void {
    const stdout = std.io.getStdOut().writer();
    // std.log.debug("count={}", .{parsed.len});
    for (parsed) |row| {
        try stdout.print("{s} {}\n", .{ NimStringData(row.user), row.uid });
    }
}

pub fn main() !void {
    c.NimMain();
    const file = try std.fs.openFileAbsolute("/etc/passwd", .{});
    const buf = try file.readToEndAlloc(std.heap.c_allocator, 4096);
    const parsed_raw = c.parse_passwd(buf.ptr, @intCast(i64, buf.len));
    const parsed = parsed_raw.p[0..parsed_raw.len];
    try print_parsed(parsed);
}
